window.onload = function(){
    var hLinks = document.querySelectorAll("#h-menu a");
    hLinks[0].classList.add("active");
    var fLinks = document.querySelectorAll("#f-menu a");
    fLinks[0].classList.add("active");

    for(var i=0; i<hLinks.length; i++) {
        hLinks[i].onclick = function(){
            var act = document.getElementsByClassName('active');
            act[0].classList.remove('active');
            this.classList.add("active");
        }
    }

    for(var j=0; j<fLinks.length; j++) {
        fLinks[j].onclick = function(){
            var act = document.getElementsByClassName('active');
            act[1].classList.remove('active');
            this.classList.add("active");
        }
    }
};